/*
Copyright (c) 2020 Carter Sifferman & Grant Bushman
*/

import 'estimate.dart';

class Folder {
  /// The name of the folder
  String name;

  /// The size of the folder, initialized later to the sum of the size of 
  /// the estimates in the folder
  double size = 0;

  /// A list of all the estimates in the folder
  List<Estimate> estimates;

  Folder(String name, List<Estimate> estimates){
    this.name = name;
    this.estimates = estimates;
    
    for (Estimate est in estimates){
      this.size += est.size;
    }
  }

  /// Add given estimates to [estimates]
  void addEstimates(List<Estimate> estimatesToAdd){
    estimates.addAll(estimatesToAdd);
  }

  /// Remove given estimates from [estimates]
  /// Uses the equality operator overriden in estimate class
  void removeEstimates(List<Estimate> estimatesToRemove){
    for (Estimate est in estimatesToRemove){
      if (estimates.contains(est)){
        estimates.remove(est);
      }
    }
  }

  /// Construct a folder from Json constructed by toJson method below
  factory Folder.fromJson(Map<String, dynamic> json){
    List<Estimate> estimates = [];
    for(var item in json['estimates']) {
      estimates.add(
        Estimate(
          item['name'] as String,
          item['size'].toDouble() as double,
          item['image'] as String,
          DateTime.parse(item['timeTaken']),
          item['location'] as String
        )
      );
    }
    return new Folder(json['name'] as String, estimates);
  }

  /// Construct Json representing the folder
  /// Estimates are stored as a list and Estimate.toJson is called implicitly
  Map<String, dynamic> toJson() =>
    {
      'name': name,
      'estimates': estimates,
    };

  // Overrides the == operator so that two estimates with the same data will
  // read as equal when compared with ==
  bool operator ==(otherFolder) => this.name == otherFolder.name && this.size == otherFolder.size;

  // Also nessecary for the equality override
  int get hashCode => name.hashCode;
}