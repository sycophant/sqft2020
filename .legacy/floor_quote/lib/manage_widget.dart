/*
Copyright (c) 2020 Carter Sifferman & Grant Bushman
*/

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:unicorndial/unicorndial.dart';

import 'estimate.dart';
import 'folder.dart';
import 'estimate_page.dart';
import 'folder_page.dart';
import 'folder_name_prompt.dart';
import 'storage.dart';
import 'move_prompt.dart';
import 'dart:io';
import 'package:pull_to_refresh/pull_to_refresh.dart';


/// Displays a list of estimates and folders
/// Allows user to manage estimates and folders
class ManageWidget extends StatefulWidget {

  /// Storage to access json files
  final Storage storage = Storage();

  ManageWidget({Key key}) : super(key: key);

  @override
  _ManageWidgetState createState() => _ManageWidgetState();
}

class _ManageWidgetState extends State<ManageWidget> {
  
  /// All of the user's estimates
  List<Estimate> _estimates = [];

  /// The currently selected (by check boxes) estimates
  final _selectedEsts = List<Estimate>();

  /// All of the user's folders
  List<Folder> _folders = [];

  /// The currently selected (by check boxes) folders
  final _selectedFolds = List<Folder>();

  String _myPath;

  /// Initialize the widget by reading in the folder and estimate jsons
  /// This ensures that it is displaying the most up-to-date information
  @override
  void initState() {
    super.initState();
    readJsons();
  }

  /// Read in both the folders and estimates json
  /// Set [_estimates] and [_folders] accordingly
  void readJsons(){
    widget.storage.readEstimatesJson().then((List<Estimate> value) {
      setState(() {
        _estimates = value;
      });
    });
    widget.storage.readFoldersJson().then((List<Folder> value) {
      setState(() {
        _folders = value;
      });
    });
    widget.storage.myPath().then((String value) {
      setState(() {
        _myPath = value;
      });
    });
  }

  /// Build a list item for the given estimates
  Widget _buildEstimate(Estimate estimate){
    
    /// Is the given estimate selected
    bool itemSelected = false;
    
    for (Estimate selected_est in _selectedEsts){
      if (selected_est == estimate){
        itemSelected = true;
      }
    }

    return ListTile(
      leading: AspectRatio(
        aspectRatio: 4/3, 
        child: Image.file(File(_myPath + "/" + estimate.timeTaken.toString()))
      ),
      title: Text(estimate.name),
      subtitle: Text(estimate.size.toString() + " sq. ft."),
      trailing: GestureDetector(
        child: Icon(itemSelected ? 
               Icons.check_box :
               Icons.check_box_outline_blank),
        onTap: () {
          setState(() {
            if (itemSelected) {
              _selectedEsts.remove(estimate);
            }
            else {
              _selectedEsts.add(estimate);
            }
          });
        },
      ),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => EstimatePage(est: estimate, path: _myPath)),
        ).then((value){ // Make sure manage page is updated when the user returns
          setState(() {
            readJsons();
          });
        });
      }
    );
  }

  /// Build a list item for the given folder
  Widget _buildFolder(Folder folder){

    /// Stores whether the given folder is selected
    bool itemSelected = false;

    for (Folder selected_fold in _selectedFolds){
      if (selected_fold == folder){
        itemSelected = true;
      }
    }

    return ListTile(
      leading: Icon(Icons.folder, size: 50),
      title: Text(folder.name),
      subtitle: Text(folder.size.toString() + " sq. ft."),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => FolderPage(fold: folder)),
        ).then((value){ // Make sure manage page is updated when the user returns
          setState(() {
            readJsons();
          });
        });
      },
      trailing: GestureDetector(
        child: Icon(itemSelected ? 
               Icons.check_box :
               Icons.check_box_outline_blank),
        onTap: () {
          setState(() {
            if (itemSelected) {
              _selectedFolds.remove(folder);
            }
            else {
              _selectedFolds.add(folder);
            }
          });
        },
      ),
    );
  }

RefreshController _refreshController =
      RefreshController(initialRefresh: false);

Future<void> _onRefresh() async{
    final Storage storage = Storage();

    // monitor network fetch
    await Future.delayed(Duration(milliseconds: 1000));

    //Refresh the JSONs and set them to the updated ones from the server
    await storage.refresh().then((value){
      setState(() {
        readJsons();
      });
    });
  
    _refreshController.refreshCompleted();
  }
  // Builds the refresh feature 
  @override
  Widget build(BuildContext context) {
    return Scaffold( 
      body: SmartRefresher(
        controller: _refreshController,
        onRefresh: _onRefresh,
        child: ListView.builder(
        padding: const EdgeInsets.all(8),
        itemCount: _estimates.length + _folders.length,
        itemBuilder: (BuildContext context, int index) {
          if (index < _estimates.length){ // Render estimates first
            return _buildEstimate(_estimates[index]);
          }
          else{
            return _buildFolder(_folders[index - _estimates.length]);
          }
        }
      ),
        ),
      floatingActionButton: UnicornDialer( // Allows access to actions
        parentButton: Icon(Icons.more_horiz),
        orientation: UnicornOrientation.VERTICAL,
        childButtons: <UnicornButton> [
          // Builds button that contains the delete feature
          UnicornButton(
            hasLabel: true,
            labelText: "Delete",
            currentButton: FloatingActionButton(
              heroTag: "delete",
              backgroundColor: Colors.redAccent,
              mini: true,
              child: Icon(Icons.delete),
              onPressed: () {
                setState(() {
                  for (Estimate est in _selectedEsts){
                    if (_estimates.contains(est)){
                      _estimates.remove(est);
                    }
                  }
                  for (Folder fold in _selectedFolds){
                    if (_folders.contains(fold)){
                      _folders.remove(fold);                    
                    }
                  }
                  widget.storage.writeEstimatesJson(_estimates);
                  widget.storage.writeFoldersJson(_folders);
                  _selectedEsts.clear();
                  _selectedFolds.clear();
                });
              }
            )
          ),
          // Builds the button to create a new folder
          UnicornButton(
            hasLabel: true,
            labelText: "Create Folder",
            currentButton: FloatingActionButton(
              heroTag: "create_folder",
              mini: true,
              child: Icon(Icons.folder),
              onPressed: () {
                // folderElements acts as a deep copy of _selected this way when we
                // clear selected we don't lose the reference in the new folder
                List<Estimate> folderElements = []..addAll(_selectedEsts);
                Folder newFolder = Folder('New Folder', folderElements);
                setState(() {
                  _folders.add(newFolder);
                  for (Estimate est in _selectedEsts){
                    _estimates.remove(est);
                    est.location = 'folder';
                  }
                  _selectedEsts.clear();
                });
                widget.storage.writeEstimatesJson(_estimates);
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => FolderNamePrompt(fold: newFolder)),
                ).then((value){ // Make sure manage page is updated when the user returns
                  setState(() {
                    readJsons();
                  });
                });
              },
            )
          ),
          // Builds button for moving an estimate into a folder
          UnicornButton(
            hasLabel: true,
            labelText: "Move",
            currentButton: FloatingActionButton(
              heroTag: "move",
              mini: true,
              child: Icon(Icons.move_to_inbox),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => MovePrompt(selectedEsts: _selectedEsts)),
                ).then((value){ // Make sure manage page is updated when the user returns
                  setState(() {
                    readJsons();
                  });
                });
              }
            )
          )
        ]
      )
    );
  }
}