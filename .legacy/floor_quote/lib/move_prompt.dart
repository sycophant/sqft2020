/*
Copyright (c) 2020 Carter Sifferman & Grant Bushman
*/

import 'package:flutter/material.dart';
import 'folder.dart';
import 'estimate.dart';
import 'storage.dart';

/// Displays a prompt asking where the user would like to move the given list
/// of estimates. Moves the given folder when a new location is tapped
class MovePrompt extends StatefulWidget{

  /// The estimates to be moved
  final List<Estimate> selectedEsts;

  /// Storage to access jsons
  final Storage storage = Storage();

  MovePrompt({Key key, this.selectedEsts}): super(key: key);

  @override
  _MovePromptState createState() => _MovePromptState();
}

class _MovePromptState extends State<MovePrompt> {

  /// A list of all of the user's estimates, as read in from json
  List<Estimate> _estimates = [];

  /// A list of all of the user's folders, as read in from json
  List<Folder> _folders = [];

  /// Read in the json files when the widget is initialized
  @override
  void initState() {
    super.initState();
    widget.storage.readEstimatesJson().then((List<Estimate> value) {
      setState(() {
        _estimates = value;
      });
    });
    widget.storage.readFoldersJson().then((List<Folder> value) {
      setState(() {
        _folders = value;
      });
    });
  }

  /// Build the given folder list item
  Widget _buildFolder(Folder folder){
    return ListTile(
      leading: Icon(Icons.folder, size: 50),
      title: Text(folder.name),
      subtitle: Text(folder.size.toString() + " sq. ft."),
      onTap: () {
        for(Estimate est in widget.selectedEsts){
          if(est.location == 'root'){
            _estimates.remove(est);
            est.location = 'folder';
            folder.addEstimates([est]);
            print(est);
          }
          else{
            for(Folder fold in _folders){
              fold.estimates.remove(est);
              print(est);
            }
            folder.addEstimates([est]);
          }
        }
        widget.storage.writeEstimatesJson(_estimates);
        widget.storage.writeFoldersJson(_folders);
        Navigator.pop(context);
      },      
    );
  }
  // Builds the widget to move an estimate out of the current folder
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
				title: Text("Move Estimates"),
        backgroundColor: Colors.blue
		 	),
      body: ListView.builder(
        padding: const EdgeInsets.all(8),
        itemCount: _folders.length+1,
        itemBuilder: (BuildContext context, int index) {
          if(index == 0){ // Root item
            return ListTile(
              leading: Icon(Icons.home, size: 50),
              title: Text("Root Directory"),
              onTap: () {
                for(Estimate est in widget.selectedEsts){
                  if(est.location == 'root'){
                    _estimates.remove(est);
                    est.location = "root";
                    _estimates.add(est);
                    print(est);
                  }
                  else{
                    for(Folder fold in _folders){
                      fold.estimates.remove(est);
                      est.location = "root";
                      _estimates.add(est);
                      print(est);
                    }
                  }
              }
              widget.storage.writeEstimatesJson(_estimates);
              widget.storage.writeFoldersJson(_folders);
              Navigator.pop(context);
              },  
            );
          }
          else{
            return _buildFolder(_folders[index-1]);
          }
        }
      )
    );
  }
}