/*
Copyright (c) 2020 Carter Sifferman & Grant Bushman
*/

import 'package:flutter/material.dart';
import 'import_widget.dart';
import 'capture_widget.dart';
import 'manage_widget.dart';

/// The app's home page, including the tab bar
/// All other widgets except for main are children of this widget
class Home extends StatefulWidget {
  Home({Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
		return _HomeState();
	}
}

class _HomeState extends State<Home> {

  /// The currently tapped tab
  int _currentIndex = 0;
	@override
	Widget build(BuildContext context) {
    /// The widgets corresponding to the tabs at the bottom
    final List<Widget> _children = [
      ImportWidget(),
      CaptureWidget(),
      ManageWidget()
    ];
    // Builds the tabs at the bottom for the different pages of the app
		return Scaffold(
			appBar: AppBar(
				title: Text('Floor Estimator'),
        backgroundColor: Colors.blue
		 	),
			body: _children[_currentIndex],
		 	bottomNavigationBar: BottomNavigationBar(
				onTap: onTabTapped,
				currentIndex: _currentIndex,
			 	items: [
				 	BottomNavigationBarItem(
					 	icon: new Icon(Icons.add_box),
					 	title: new Text('Import'),
				 	),
				 	BottomNavigationBarItem(
					 	icon: new Icon(Icons.camera_alt),
					 	title: new Text('Take Photos'),
				 	),
				 	BottomNavigationBarItem(
					 	icon: Icon(Icons.apps),
					 	title: Text('Manage')
				 	)
			 	],
		 	),
	 	);
 	}
  /// When a tab is tapped, display its corresponding widget
	void onTabTapped(int index) {
		setState(() {
			_currentIndex = index;
		});
	}
}