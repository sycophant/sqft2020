/*
Copyright (c) 2020 Carter Sifferman & Grant Bushman
*/

import 'package:flutter/material.dart';
import 'folder.dart';
import 'storage.dart';

/// The prompt to give a new name to a folder upon initial creation
/// If the user hits back instead of giving a name, a default name is given
class FolderNamePrompt extends StatefulWidget{

  /// The folder to rename
  final Folder fold;

  const FolderNamePrompt({Key key, this.fold}): super(key: key);

  @override
  _FolderNamePromptState createState() => _FolderNamePromptState();
}

class _FolderNamePromptState extends State<FolderNamePrompt> {

  final myController = TextEditingController();
  final storage = Storage();

  /// Clean up the controller when the widget is disposed.
  @override
  void dispose() {
    myController.dispose();
    super.dispose();
  }

  // Builds the widget used to create a new folder for estimates
  @override
  Widget build(BuildContext context) {
    return Scaffold(
			appBar: AppBar(
				title: Text("Name your new folder"),
        backgroundColor: Colors.blue
		 	),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          children: [
            TextField(
              controller: myController,
            ),
            FlatButton(
              child: Text("Create Folder"),
              onPressed: () {
                storage.readFoldersJson().then((List<Folder> value) {
                  List<Folder> folders = value;
                  widget.fold.name = myController.text;
                  folders.add(widget.fold);
                  storage.writeFoldersJson(folders);
                  Navigator.pop(context);                  
                });
              }
            )
          ]
        )
      )
    );
  }
}