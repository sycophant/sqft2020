/*
Copyright (c) 2020 Carter Sifferman & Grant Bushman
*/

import 'package:flutter/material.dart';
import 'home_widget.dart';
import 'storage.dart';
import 'estimate.dart';
import 'folder.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

main() {
  /// Initialize storage to write to json
  Storage storage = Storage();

  storage.refresh();

  runApp(new App());
}

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'My Flutter App',
      home: Home(),
    );
  }
}

