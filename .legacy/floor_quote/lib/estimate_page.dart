/*
Copyright (c) 2020 Carter Sifferman & Grant Bushman
*/

import 'package:flutter/material.dart';
import 'estimate.dart';
import 'rename_estimate_page.dart';
import 'package:intl/intl.dart';
import 'storage.dart';
import 'dart:io';

/// Displays details and image of the given estimate
class EstimatePage extends StatefulWidget {
  
  /// The estimate whose details should be displayed
  final Estimate est;

  // local path where image is stored
  final String path;

  const EstimatePage({Key key, this.est, this.path}): super(key: key);
    
  @override
  _EstimatePageState createState() => _EstimatePageState();
}

class _EstimatePageState extends State<EstimatePage> {
  final Storage storage = Storage();

  // This builds the actual estimate using data provided by the JSON file
  @override
  Widget build(BuildContext context) {
		return Scaffold(
			appBar: AppBar(
				title: Text(widget.est.name),
        backgroundColor: Colors.blue
		 	),
			body: Column(
        children: <Widget>[
          Image.file(
            File(widget.path + "/" + widget.est.timeTaken.toString()),
            width: 600,
            height: 240,
            fit: BoxFit.cover,
          ),
          Container(
            padding: const EdgeInsets.all(32),
            child: Row(
              children: [
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        padding: const EdgeInsets.only(bottom: 8),
                        child: Text(
                          widget.est.name,
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      Text(
                        widget.est.size.toString() + " sq. ft.",
                        style: TextStyle(
                          color: Colors.grey[500],
                        ),
                      ),
                      Text(
                        "Taken " + new DateFormat.yMd().add_jm().format(widget.est.timeTaken),
                        style: TextStyle(
                          color: Colors.grey[500],
                        ),
                      ),
                      FlatButton(
                        child: Text("Rename Estimate"),
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(builder: (context) => RenameEstimatePage(est: widget.est)),
                          );
                        }
                      )
                    ],
                  ),
                ),
              ],
            ),
          )
        ],
      )
    );
  }
}