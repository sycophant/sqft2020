/*
Copyright (c) 2020 Carter Sifferman & Grant Bushman
*/

import 'package:flutter/material.dart';
import 'estimate.dart';
import 'storage.dart';
import 'folder.dart';

/// Displays a dialog asking for a new name for the given folder
/// Renames the given folder with the name recieved inthe dialog
class RenameEstimatePage extends StatefulWidget{

  final Estimate est;

  const RenameEstimatePage({Key key, this.est}) : super(key: key);

  @override
  _RenameEstimatePageState createState() => _RenameEstimatePageState();
}

class _RenameEstimatePageState extends State<RenameEstimatePage> {

  /// Storage to access Jsons
  final Storage storage = Storage();

  /// Controller for text input
  final myController = TextEditingController();

  /// Clean up the controller when the widget is disposed.
  @override
  void dispose() {
    myController.dispose();
    super.dispose();
  }
  // Builds widget to name/rename an estimate
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            title: Text("Name Estimate '" + widget.est.name + "'"),
            backgroundColor: Colors.blue),
        body: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(children: [
              TextField(
                controller: myController,
              ),
              FlatButton(
                  child: Text("Name Estimate"),
                  onPressed: () {
                    if (widget.est.location == 'root') {
                      //search through the estimates storage
                      storage.readEstimatesJson().then((List<Estimate> value) {
                        List<Estimate> estimates = value;
                        if (estimates.contains(widget.est)) {
                          estimates.remove(widget.est);
                          widget.est.name = myController.text;
                          estimates.add(widget.est);
                          storage.writeEstimatesJson(estimates);
                        }
                      });
                    } else {
                      //search through the folders storage
                      storage.readFoldersJson().then((List<Folder> value2) {
                        List<Folder> folders = value2;
                        for (Folder fold in folders) {
                          if (fold.estimates.contains(widget.est)) {
                            fold.estimates.remove(widget.est);
                            widget.est.name = myController.text;
                            fold.estimates.add(widget.est);
                            storage.writeFoldersJson(folders);
                          }
                        }
                      });
                    }
                    Navigator.pop(context);
                  })
            ])));
  }
}
