/*
Copyright (c) 2020 Carter Sifferman & Grant Bushman
*/

import 'dart:async';
//import 'dart:ffi';
import 'dart:io';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';

import 'package:path_provider/path_provider.dart';

import 'estimate.dart';
import 'folder.dart';

/// Used to access json files
class Storage {

  String serverIP = "http://ec2-18-217-134-54.us-east-2.compute.amazonaws.com:3000";

  /// Returns the application directory, as given by the device
  Future<String> get _localPath async {
    final directory = await getApplicationDocumentsDirectory();

    return directory.path;
  }

  Future<String> myPath() async {
    return _localPath;
  }

  /// Gets the estimates file as a file
  Future<File> get _estimatesFile async {
    final path = await _localPath;
    return File('$path/estimates.json');
  }

  /// Gets the folders file as a file
  Future<File> get _foldersFile async {
    final path = await _localPath;
    return File('$path/folders.json');
  }

  /// Reads in _estimatesFile as a list of estimates
  /// Returns a list of Estimates
  Future<List<Estimate>> readEstimatesJson() async {
    try {
      // The file to be parsed
      final file = await _estimatesFile;

      // Read the file and parse to json
      String contents = await file.readAsString();

      final parsed = json.decode(contents).cast<Map<String, dynamic>>();
      return parsed.map<Estimate>((json) => new Estimate.fromJson(json)).toList();
    }
    catch (e) {
      // If encountering an error, return []
      return [];
    }
  }

  /// Reads in _foldersFile as a list of estimates
  /// Returns a list of folders
  Future<List<Folder>> readFoldersJson() async {
    try{
      // The file to be parsed
      final file = await _foldersFile;

      // Read the file and parse to Json
      String contents = await file.readAsString();

      final parsed = json.decode(contents).cast<Map<String, dynamic>>();
      return parsed.map<Folder>((json) => new Folder.fromJson(json)).toList();
    } 
    catch (e) {
      // If encountering an error, return []
      return [];
    }
  }

  /// Reads in a single folder json
  /// Really reads in the whole foldersjson and just returns the relevant one
  /// So this isn't any more effecient, just more convenient
  Future<Folder> readSingleFolderJson(Folder fold) async {
    try{
      // The folder file to be read in
      final file = await _foldersFile;

      // The contents of the folder file as a string
      String contents = await file.readAsString();

      // Parse it to a list of folder objects
      final parsed = json.decode(contents).cast<Map<String, dynamic>>();
      final list = parsed.map<Folder>((json) => new Folder.fromJson(json)).toList();

      for(Folder f in list){
        if(f.name == fold.name){
          return f;
        }
      }
      return null;
    } 
    catch (e) {
      // If encountering an error, return null
      return null;
    }
  }

  /// Write the given list of estimates to the estimates json file
  Future<File> writeEstimatesJson(List<Estimate> estimates, {bool upload = true}) async {
    final file = await _estimatesFile;

    String estimatesJson = jsonEncode(estimates).toString();
    if(upload){
      String url = serverIP + "/estimates";
      Map<String, String> headers = {"Content-type": "application/json"};
      http.post(url, headers: headers, body: estimatesJson);
    }

    return file.writeAsString(estimatesJson);
  }

  /// Write the given list of folders to the folders json file
  Future<File> writeFoldersJson(List<Folder> folders, {bool upload = true}) async {
    final file = await _foldersFile;

    String foldersJson = jsonEncode(folders).toString();

    if(upload){
      String url = serverIP + "/folders";
      Map<String, String> headers = {"Content-type": "application/json"};
      http.post(url, headers: headers, body: foldersJson);
    }

    return file.writeAsString(foldersJson);
  }

  /// Upload img to remote server
  /// Returns "Upload failed" if failed, "Uploaded!" if successful
  Future<String> uploadImage(File img) async {
    String url = serverIP + "/upload";
    var request = new http.MultipartRequest("POST", Uri.parse(url));

    request.fields['name'] = 'test_name';
    request.files.add(await http.MultipartFile.fromPath(
        'file',
        img.path,
    ));
    request.send().then((response) {
      if (response.statusCode == 200) return("Uploaded!");
      return("Upload failed");
    });
    return("Upload failed");
  }

  /// Get a json list of images available on the remote server
  Future<List<dynamic>> listImages() async {
    String url = serverIP + "/listimages";
    List<dynamic> images;
    await http.get(url).then((response){
      images = json.decode(response.body).toList();
    });
    return images;
  }

  /// Get a single image from remote server
  /// Image is saved to the local path under the same name as was requested
  Future<void> getImage(String name) async {
    print('getting image ' + name);
    String url = serverIP + "/images/" + name;
    var response = await http.get(url);

    final path = await _localPath;
    String localImagePath = path + "/" + name;
    File file = new File(localImagePath);
    file.writeAsBytesSync(response.bodyBytes);
  }

  /// Move sourceFile to a more permanent location
  /// The file's name will be the time given by time, which matches the time
  /// for the corresponding estimate
  /// 
  /// Also triggers uploadImage to upload sourceFile to server
  Future<File> moveFile(File sourceFile, DateTime time) async {
        
    final path = await _localPath;
    try {
      // prefer using rename as it is probably faster
      return await sourceFile.rename(path + "/" + time.toString());
    } on FileSystemException catch (e) {
      // if rename fails, copy the source file and then delete it
      final newFile = await sourceFile.copy(path + "/" + time.toString());
      await sourceFile.delete();
      return newFile;
    }
  }
  // Refresh feature
  Future<void> refresh() async{
    List<String> localFiles = [];
    Directory dir = await getApplicationDocumentsDirectory();
    dir.list(recursive: false, followLinks: false)
      .listen((FileSystemEntity entity) {
        localFiles.add(entity.path.split("/").last);
    });

    final images = await listImages();
    // Gets JSON from server, then decodes JSON to build estimates
    await http.get(serverIP + "/estimates").then((value){
      if (value.body.toString() != "{}"){
        final parsed = json.decode(value.body).cast<Map<String, dynamic>>();
        writeEstimatesJson(parsed.map<Estimate>((json) => new Estimate.fromJson(json)).toList(), upload: false);
      }
    });
    // Gets JSON from server, then decodes JSON to build folders
    await http.get(serverIP + "/folders").then((value){
      if (value.body.toString() != "{}"){
        final parsed = json.decode(value.body).cast<Map<String, dynamic>>();
        writeFoldersJson(parsed.map<Folder>((json) => new Folder.fromJson(json)).toList(), upload: false);
      }
    });
    // checks to make sure every estimate has an associated image
    for (String img in images){
      if (!localFiles.contains(img)){
        await getImage(img);
      }
    }
  }
}
