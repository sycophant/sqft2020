/*
Copyright (c) 2020 Carter Sifferman & Grant Bushman
*/

import 'package:flutter/material.dart';
import 'folder.dart';
import 'estimate.dart';
import 'estimate_page.dart';
import 'rename_folder_page.dart';
import 'package:unicorndial/unicorndial.dart';
import 'storage.dart';
import 'move_prompt.dart';

/// Displays a folder's contents
class FolderPage extends StatefulWidget {

  /// The folder whose contents to display
  final Folder fold;

  const FolderPage({Key key, this.fold}): super(key: key);

  @override
  _FolderPageState createState() => _FolderPageState();
}
  
class _FolderPageState extends State<FolderPage> {

  /// Used to access the Json files in the phone's storage
  final Storage storage = Storage();

  /// A list of all of the estimates in the folder currently selected via the
  /// checkbox on the right side
  final _selected = List<Estimate>();

  /// A list of all of the estimates in the folder
  List<Estimate> _estimates = List<Estimate>();

  @override
  void initState() {
    super.initState();
    _estimates = widget.fold.estimates;
  }

  /// Returns a single estimate list item
  Widget _buildEstimate(Estimate estimate){
    /// Keeps track of if the current item is selected
    bool itemSelected = false;
    
    //filters through selected items to make sure they are estimates
    for (Estimate selected_est in _selected){
      if (selected_est == estimate){
        itemSelected = true;
      }
    }
    // builds each estimate within the folder, adding image, name, etc
    return ListTile(
      leading: AspectRatio(
        aspectRatio: 4/3, 
        child:Image.asset(estimate.image)
      ),
      title: Text(estimate.name),
      subtitle: Text(estimate.size.toString() + " sq. ft."),
      trailing: GestureDetector(
        child: Icon(itemSelected ? 
               Icons.check_box :
               Icons.check_box_outline_blank),
        onTap: () {
          setState(() {
            if (itemSelected) {
              _selected.remove(estimate);
            }
            else {
              _selected.add(estimate);
            }
          });
        },
      ),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => EstimatePage(est: estimate)),
        );
      }
    );
  }

  // Makes sure all estimates are added to folder then prompts user to name folder
  @override
  Widget build(BuildContext context) {
		return Scaffold(
			appBar: AppBar(
				title: Text(widget.fold.name),
        backgroundColor: Colors.blue
		 	),
      body: ListView.builder(
        padding: const EdgeInsets.all(8),
        itemCount: _estimates.length +1,
        itemBuilder: (BuildContext context, int index) {
            if (index < _estimates.length){
              return _buildEstimate(_estimates[index]);
            }
            else{
              return ListTile(
                title: Text("Rename Folder"),
                leading: Icon(Icons.edit, size: 40),
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => RenameFolderPage(fold: widget.fold)),
                  );
                }
              );
            }
        }
      ),
    floatingActionButton: _selected.isEmpty ?
      null :
      // Buttons to either delete or move an estimate from the folder
      UnicornDialer(
        parentButton: Icon(Icons.more_horiz),
        orientation: UnicornOrientation.VERTICAL,
        childButtons: <UnicornButton> [
          UnicornButton(
            hasLabel: true,
            labelText: "Delete",
            currentButton: FloatingActionButton(
              heroTag: "delete",
              backgroundColor: Colors.redAccent,
              mini: true,
              child: Icon(Icons.delete),
              onPressed: () {
                setState(() {
                  storage.readFoldersJson().then((List<Folder> value) {
                    List<Folder> folders = value;
                    folders.remove(widget.fold);
                    for (Estimate est in _selected){
                      _estimates.remove(est);
                    }
                    folders.add(widget.fold);
                    storage.writeFoldersJson(folders);
                    _selected.clear();
                  });
                });
              }
            )
          ),
          UnicornButton(
            hasLabel: true,
            labelText: "Move",
            currentButton: FloatingActionButton(
              heroTag: "move",
              mini: true,
              child: Icon(Icons.move_to_inbox),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => MovePrompt(selectedEsts: _selected)),
                ).then((value){
                  storage.readSingleFolderJson(widget.fold).then((value){
                    setState((){
                      _estimates = value.estimates;
                    });
                  });
                });
              }
            )
          )
        ]
      ),
    );
  }
}