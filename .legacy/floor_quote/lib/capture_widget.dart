/*
Copyright (c) 2020 Carter Sifferman & Grant Bushman
*/

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

import 'dart:async';
import 'dart:io';
import 'storage.dart';
import 'estimate.dart';
import 'rename_estimate_page.dart';

/// Displays a button saying "Take a photo"
/// Upon being pressed, the device's camera dialog is opened
/// After a photo is taken, it is saved in the _image variable and displayed
class CaptureWidget extends StatefulWidget {
  @override
  _CaptureWidgetState createState() => _CaptureWidgetState();
}

class _CaptureWidgetState extends State<CaptureWidget> {
  
  /// The image taken by the user. Initialized to null
  File _image;

  Storage storage = new Storage();

  /// Calls and displays the system's camera
  /// Returns the image the user has taken
  Future getImage() async {
    var image = await ImagePicker.pickImage(source: ImageSource.camera);

    var now = DateTime.now();

    // Move the image to a more permanent directory
    image = await storage.moveFile(image, now);
    storage.uploadImage(image);
    
    // Reset the state to update image
    setState(() {
      _image = image;
    });
    
    /// Add image to manage page
    storage.readEstimatesJson().then((List<Estimate> estimates){
      Estimate newEstimate = Estimate("NewEstimate", 500.0, _image.path, now, "root");
      estimates.add(newEstimate);
      storage.writeEstimatesJson(estimates);
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => RenameEstimatePage(est: newEstimate)),
      );
    });
  }

  // build widget used to take a picture
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: FlatButton(
          onPressed: getImage,
          child: Text("Take a photo")
        ),
      )
    );
  }
}

