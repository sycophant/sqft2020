/*
Copyright (c) 2020 Carter Sifferman & Grant Bushman
*/

import 'package:flutter/material.dart';
import 'folder.dart';
import 'storage.dart';

/// Prompt to rename the given folder
/// Renames folder after input is recieved
/// If user backs up, folder retains original name
class RenameFolderPage extends StatefulWidget{

  /// The folder to be renamed
  final Folder fold;

  const RenameFolderPage({Key key, this.fold}): super(key: key);

  @override
  _RenameFolderPageState createState() => _RenameFolderPageState();
}

class _RenameFolderPageState extends State<RenameFolderPage> {

  /// Storage for accessing jsons
  final Storage storage = Storage();

  /// Text controller for user input
  final myController = TextEditingController();

  /// Clean up the controller when the widget is disposed.
  @override
  void dispose() {
    myController.dispose();
    super.dispose();
  }
  // Builds widget to rename a folder
  @override
  Widget build(BuildContext context) {
    return Scaffold(
			appBar: AppBar(
				title: Text("Rename Folder '" + widget.fold.name + "'"),
        backgroundColor: Colors.blue
		 	),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          children: [
            TextField(
              controller: myController,
            ),
            FlatButton(
              child: Text("Rename Folder"),
              onPressed: () {
                storage.readFoldersJson().then((List<Folder> value) {
                  List<Folder> folders = value;
                  folders.remove(widget.fold);
                  widget.fold.name = myController.text;
                  folders.add(widget.fold);
                  storage.writeFoldersJson(folders);
                });
                Navigator.pop(context);
              }
            )
          ]
        )
      )
    );
  }
}