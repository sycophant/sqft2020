/*
Copyright (c) 2020 Carter Sifferman & Grant Bushman
*/

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

import 'dart:async';
import 'dart:io';
import 'estimate.dart';
import 'storage.dart';
import 'rename_estimate_page.dart';

/// Displays a button saying "Import a photo"
/// Upon being pressed, the device's camera library dialog is opened
/// After a photo is taken, it is saved in the _image variable and displayed
class ImportWidget extends StatefulWidget {
  @override
  _ImportWidgetState createState() => _ImportWidgetState();
}

class _ImportWidgetState extends State<ImportWidget> {

  /// The image taken by the user. Initialized to null
  File _image;

  Storage storage = new Storage();

  /// Calls and displays the system's camera
  /// Returns the image the user has taken
  Future getImage() async {
    var image = await ImagePicker.pickImage(source: ImageSource.gallery);
    
    var now = DateTime.now();

    // Move the image to a more permanent directory
    image = await storage.moveFile(image, now);
    storage.uploadImage(image);

    setState(() {
      _image = image;
    });
    
    /// Add image to manage page
    storage.readEstimatesJson().then((List<Estimate> estimates){
      Estimate newEstimate = Estimate("New Estimate", 500.0, _image.path, now, "root");
      estimates.add(newEstimate);
      storage.writeEstimatesJson(estimates);
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => RenameEstimatePage(est: newEstimate)),
      );
    });
  }
  // Builds widget used to import a photo from the camera roll
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: FlatButton(
          onPressed: getImage,
          child: Text("Pick a photo")
        ),
      )
    );
  }
}

