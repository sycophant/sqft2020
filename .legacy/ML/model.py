<<<<<<< HEAD
import glob, os
from skimage import io, color
from skimage.transform import rescale
=======
# TODO save image of training curves after training
# TODO make a measure of accuracy other than r squared
# TODO command line parameters
# TODO ability to load model and resume training
# TODO augment data

import glob
import os
from skimage import io
>>>>>>> master
import torch
import torch.nn as nn
import csv
<<<<<<< HEAD
from du.lib import train, get_device, center, normalize, r_squared
from du.conv.models import TwoMetaCNN

DATASET_PATH = '/Users/grantbushman/Desktop/DLFiles/Structured3D_panos_full_rgb_rawlight_25'
TRAINING_PERCENT = 0.8
NUM_IMAGES = 10000  # how many images to pull in (max 21835)
device = get_device()
print(device)

xss = torch.Tensor(NUM_IMAGES, 32, 64)

print("importing photos")
i = 0
for file in sorted(glob.glob(DATASET_PATH + "/*.png")):
    if i < NUM_IMAGES - 1:
        img_data = color.rgb2gray(io.imread(file))
        img_data = rescale(img_data, .25, anti_aliasing=False, multichannel=False)
        xss[i] = torch.Tensor((img_data[:, :]).reshape(32, 64))
        i += 1
        print(i, "/", NUM_IMAGES, "\r", end="")
    else:
        break

yss = torch.Tensor(NUM_IMAGES, 1)
=======
import random
import statistics
import matplotlib.pyplot as plt
from datetime import datetime
from skimage.transform import rescale
from du.lib import get_device, r_squared

'''
Loads in random images from a list of image paths

images: a list of (full) image paths to load in
batch_size: how many randomly selected images to pull in
'''
def load_batch(images, batch_size=0):
  
  # if no parameter is given, just return the full batch
  if batch_size == 0:
    batch_size = len(images)

  # allocate memory for batch xss and batch yss tensors
  xss = torch.Tensor(batch_size, 3, img_height, img_width).to(device)
  yss = torch.Tensor(batch_size, 1).to(device)

  # select batch_size images from the given images
  selected_images = random.sample(images, batch_size)
>>>>>>> master

  # load in batch xss tensor from image files
  i = 0
  for img in selected_images:
    img_data = rescale(io.imread(img), IMG_SCALE, multichannel=True)
    xss[i] = torch.Tensor((img_data[:,:,:-1]).reshape(3,img_height,img_width))
    i += 1

  # load in batch yss tensor from csv file
  i = 0
  for img in selected_images:
    img_id = img.split("/")[-1].split(".")[0]
    img_id = img_id.split("_")[0] + "_" + img_id.split("_")[1] # retrieve just the ID part
    yss[i] = (float(area_lookup[img_id]) - yss_mean) / yss_stdev # mean center and normalize
    i += 1

<<<<<<< HEAD

cutoff = int(TRAINING_PERCENT * len(xss))
xss_train = xss[:cutoff]
yss_train = yss[:cutoff]
xss_test = xss[cutoff:]
yss_test = yss[cutoff:]

xss_train, xss_means = center(xss_train)
xss_train, xss_std = normalize(xss_train)

yss_train, yss_means = center(yss_train)
yss_train, yss_std = normalize(yss_train)

xss_test, _ = center(xss_test, xss_means)
xss_test, _ = normalize(xss_test, xss_std)

yss_test, _ = center(yss_test, yss_means)
yss_test, _ = normalize(yss_test, yss_std)


class ConvolutionalModel(nn.Module):

    def __init__(self):
        super(ConvolutionalModel, self).__init__()
        self.meta_layer1 = nn.Sequential(
            nn.Conv2d(in_channels=1, out_channels=16, kernel_size=5, stride=1, padding=2),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=2, stride=2, padding=0)
        )
        self.meta_layer2 = nn.Sequential(
            nn.Conv2d(in_channels=16, out_channels=32, kernel_size=5, stride=1, padding=2),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=2, stride=2, padding=0)
        )
        self.fc_layer1 = nn.Linear(4096, 512)   #the 131072 comes from 16 out_channels * 64x128 which is the size
        self.fc_layer2 = nn.Linear(512,1)        #of the image after Max Pooling

    def forward(self, xss):
        xss = torch.unsqueeze(xss, dim=1)
        xss = self.meta_layer1(xss)
        xss = self.meta_layer2(xss)
        xss = torch.reshape(xss, (-1, 4096))
        xss = torch.relu(xss)
        xss =  self.fc_layer1(xss)
        xss = torch.relu(xss)
        xss = self.fc_layer2(xss)
        return xss

=======
  return xss, yss

'''
Convolutional Model
'''
class ConvolutionalModel(nn.Module):

  def __init__(self):

    super(ConvolutionalModel, self).__init__()

    self.meta_layer1 = nn.Sequential(
      nn.Conv2d(in_channels=3, out_channels=16, kernel_size=5, stride=1, padding = 2),
      nn.ReLU(),
      nn.MaxPool2d(kernel_size = 2, stride = 2, padding = 0)
    )

    self.meta_layer2 = nn.Sequential(
      nn.Conv2d(in_channels=16, out_channels=32, kernel_size=5, stride=1, padding=2),
      nn.ReLU(),
      nn.MaxPool2d(kernel_size = 2, stride = 2, padding = 0)
    )

    self.meta_layer3 = nn.Sequential(
      nn.Conv2d(in_channels=32, out_channels=64, kernel_size=5, stride=1, padding=2),
      nn.ReLU(),
      nn.MaxPool2d(kernel_size = 2, stride = 2, padding = 0)
    )

    self.fc_layer1 = nn.Linear(int(32768 * IMG_SCALE * IMG_SCALE * 4), 5000)
    self.fc_layer2 = nn.Linear(5000, 100)
    self.fc_layer3 = nn.Linear(100, 1)

  def forward(self, xss):
    xss = self.meta_layer1(xss)
    xss = self.meta_layer2(xss)
    xss = self.meta_layer3(xss)
    xss = torch.reshape(xss, (-1, xss.shape[1]*xss.shape[2]*xss.shape[3]))
    xss = self.fc_layer1(xss)
    xss = self.fc_layer2(xss)
    return self.fc_layer3(xss)
>>>>>>> master

'''
Parameters
'''
DATASET_PATH = "/home/sharedData/Structured3D_panos_full_rgb_rawlight_50_flip_roll4_all"
TRAINING_PERCENT = 0.95
IMG_SCALE = 1 / 16
MS_NORMALIZE_YSS = True
NOW = datetime.now().strftime("%Y-%m-%d_%H-%M")

img_height = int(IMG_SCALE * 256)
img_width = int(IMG_SCALE * 512)

criterion = nn.MSELoss()
<<<<<<< HEAD
epochs = 10
learning_rate = 0.0001
momentum = .99
batchsize = 20

model = train(
    model,
    criterion,
    (xss_train, yss_train),
    test_data=(xss_test, yss_test),
    epochs=epochs,
    learn_params={"lr": learning_rate, "mo": momentum},
    graph=1,
    bs=batchsize
)

yhats = model(xss_train)
pcorrect = r_squared(yhatss=yhats, yss=yss_train)
print("Percent Correct (Training): " + str(int(pcorrect*100)) + '%')

testcorrect = r_squared(model(xss_test), yss_test)
print("Percent Correct (Testing): " + str(int(testcorrect*100)) + '%')
=======
epochs = 30
learning_rate = 0.003
momentum = 0.99
batch_size = 25

'''
Main Program
'''
# change directory to directory of current file
os.chdir(os.path.dirname(os.path.abspath(__file__)))

device = get_device()

# get the mean and standard deviation of the room areas (yss)
areas = []
with open(DATASET_PATH + "/ground_truth.csv") as csvfile:
  ground_truth = list(csv.reader(csvfile))
  for example in ground_truth:
    areas.append(float(example[2]))

if MS_NORMALIZE_YSS:
  yss_mean = sum(areas) / len(areas)
  yss_stdev = statistics.stdev(areas)
else:
  yss_mean = 0
  yss_stdev = 1

# create a dictionary of the csv file with key ID and value area
# this allows more time-efficient lookups
with open(DATASET_PATH + "/ground_truth.csv") as csvfile:
  csvfile = list(csv.reader(csvfile))
  area_lookup = dict()
  for row in csvfile:
    area_lookup[row[1]] = row[2]

# get all of the images in the dataset directory
all_images = glob.glob(DATASET_PATH + "/*.png")

# the cutoff where training ends and testing begins
cutoff = int(TRAINING_PERCENT * len(all_images))

# select training and testing images
train_images = all_images[:cutoff]
test_images = all_images[cutoff:]

model = ConvolutionalModel().to(device)

previous_grad = [0 for param in model.parameters()]

past_losses = []
avg_past_losses = []

train_r_squared = []
test_r_squared = []

# create directories to save models information
save_dir = "./" + NOW
if not os.path.exists(save_dir):
  os.mkdir(save_dir)
model_dir = save_dir + "/models"
if not os.path.exists(model_dir):
  os.mkdir(model_dir)

for epoch in range(epochs):  # train the model

  for i in range(len(train_images) // batch_size):

    # chunk through train_images
    start_index = i*batch_size
    end_index = (i+1)*batch_size
    batch_train_images = train_images[start_index:end_index]

    # get chunked out images
    batch_xss, batch_yss = load_batch(batch_train_images)

    # get the train data loss
    yss_pred = model(batch_xss)
    loss = criterion(yss_pred, batch_yss) # compute the loss

    # calculate average of last 50 losses
    past_losses.append(loss.item())
    avg_loss = sum(past_losses[-50:]) / len(past_losses[-50:])
    avg_past_losses.append(avg_loss)

    # print info
    print("epoch: {} ({} / {}) loss: {:<20} avg of last 50: {}\r"
      .format(
        epoch+1,
        i,
        len(train_images) // batch_size,
        loss.item(),
        avg_loss
      ), end=""
    )

    model.zero_grad() # set the gradient to the zero vector
    loss.backward() # compute the gradient of the loss function w/r to the weights

    # adjust the weights
    i = 0
    for param in model.parameters():
      param.data.sub_(learning_rate * (momentum * previous_grad[i] + param.grad.data))
      i += 1

    previous_grad = [param.grad.data for param in model.parameters()]

  # shuffle the images between epochs
  random.shuffle(train_images)

  # save model to disk
  save_path = model_dir + "/epoch_" + str(epoch+1) + ".pyt"
  torch.save(model.state_dict(), save_path)

  # load saved model on the cpu so we can run more data through it at once
  device = 'cpu'
  model = ConvolutionalModel().to(device)
  model.load_state_dict(torch.load(save_path))
  model.eval()

  # calculate the r squared of the testing data
  print("\nCalculating (subset of) training r squared...")
  train_xss, train_yss = load_batch(train_images, 1000)
  train_r_squared.append(r_squared(model(train_xss), train_yss))
  print("Training r squared:", train_r_squared[-1])

  # calculate the r squard of the testing data
  print("Calculating testing r squared...")
  test_xss, test_yss = load_batch(test_images)
  test_r_squared.append(r_squared(model(test_xss), test_yss))
  print("Testing r squared:", test_r_squared[-1])

  # load the model in again on the gpu if available
  device = get_device()
  model = ConvolutionalModel().to(device)
  model.load_state_dict(torch.load(save_path))
  model.eval()

with open(save_dir + "/model.txt", "w+") as f:
  f.write("Model created on " + NOW + "\n\n")
  f.write("Total epochs: " + str(epochs) + "\n")
  f.write("Learning rate: " + str(learning_rate) + "\n")
  f.write("Momentum: " + str(momentum) + "\n")
  f.write("Batch size: " + str(batch_size) + "\n")
  f.write("\n")

  f.write("Dataset path: " + DATASET_PATH + "\n")
  f.write("Image scale: " + str(IMG_SCALE) + " (" + str(img_width) + " x " +
          str(img_height) + ")\n")
  f.write("Training percent: " + str(TRAINING_PERCENT) + "\n")
  f.write("Number of total training images: " + str(len(train_images)) + "\n")
  f.write("Mean center and normalize yss: " + str(MS_NORMALIZE_YSS) + "\n")
  f.write("\n")

  best_train_epoch = train_r_squared.index(max(train_r_squared))
  f.write("Best epoch on training data: " + str(best_train_epoch + 1) + " (" +
          str(train_r_squared[best_train_epoch]) + ")" + "\n")

  best_test_epoch = test_r_squared.index(max(test_r_squared))
  f.write("Best epoch on testing data: " + str(best_test_epoch + 1) + " (" +
          str(test_r_squared[best_test_epoch]) + ")" + "\n")

  f.write("Training curves\n")
  f.write("Training r-squared\n")
  for i in range(len(train_r_squared)):
    f.write("epoch " + str(i+1) + ": " + str(train_r_squared[i]) + "\n")
  f.write("\n")

  f.write("Testing r-squared\n")
  for i in range(len(test_r_squared)):
    f.write("epoch " + str(i+1) + ": " + str(test_r_squared[i]) + "\n")

# plot r squared for each epoch
plt.subplot(2,1,1)
plt.plot(range(len(test_r_squared)), test_r_squared)
plt.plot(range(len(train_r_squared)), train_r_squared)
plt.legend(["testing r squared", "training r squared"])

# plot average of past 50 losses over time
plt.subplot(2,1,2)
plt.plot(range(len(avg_past_losses)), avg_past_losses)
plt.legend(["loss"])

# show plots
plt.show()
>>>>>>> master
