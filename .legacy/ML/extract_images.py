import os
import sys
import json
import argparse
import csv
import glob
from shutil import copyfile

import numpy as np
from shapely.geometry import Polygon
import PIL
from PIL import Image
from PIL import ImageOps

'''
Copy and downscale images from a directory containing the entire dataset to 
another directory
Does not modify dataset in --input
Data in --output is organized more simply - each image has an ID (its file name)
The ground_truth.csv file in --output contains each ID's room type and area

Example: python3 extract_images.py --input /media/carter/Data/already_extracted/Structured3D/ --output /home/carter/Desktop/ --scale 0.5
'''

def convert_lines_to_vertices(lines):
  polygons = []
  lines = np.array(lines)

  polygon = None
  while len(lines) != 0:
    if polygon is None:
      polygon = lines[0].tolist()
      lines = np.delete(lines, 0, 0)

    lineID, juncID = np.where(lines == polygon[-1])
    vertex = lines[lineID[0], 1 - juncID[0]]
    lines = np.delete(lines, lineID, 0)

    if vertex in polygon:
      polygons.append(polygon)
      polygon = None
    else:
      polygon.append(vertex)

  return polygons

def get_sqft(json, scene):
  output = ""

  # extract the floor in each semantic for floorplan visualization
  room_types = ["living room", "kitchen", "bedroom", "bathroom", "balcony", "corridor", "dining room", "study", "studio", "store room", "garden", "laundry room", "office", "basement", "garage", "undefined"]

  planes = []
  for semantic in json['semantics']:
    if semantic['type'] in room_types:
      for planeID in semantic['planeID']:
        if json['planes'][planeID]['type'] == 'floor':
          planes.append({'planeID': planeID, 'type': semantic['type'], 'ID': semantic['ID']})

  # construct each polygon
  polygons = []
  for plane in planes:
    lineIDs = np.where(np.array(json['planeLineMatrix'][plane['planeID']]))[0].tolist()
    junction_pairs = [np.where(np.array(json['lineJunctionMatrix'][lineID]))[0].tolist() for lineID in lineIDs]
    polygon = convert_lines_to_vertices(junction_pairs)
    polygons.append([polygon[0], plane['type'], plane['ID']])

  junctions = np.array([junc['coordinate'][:2] for junc in json['junctions']])

  for (polygon, poly_type, ID) in polygons:
    polygon = Polygon(junctions[np.array(polygon)])
    output = output + str(poly_type) + "," + str(scene) + "_" + str(ID) + "," + str(polygon.area/(276*276)) + "\n"

  return output # return output without the extra newline character

def parse_args():
  parser = argparse.ArgumentParser(description="Extracts Images from Structured 3D Dataset")
  parser.add_argument("--input", required=True,
                      help="path to full dataset", metavar="DIR")
  parser.add_argument("--output", required=True,
                  help="where to output data (will create a folder in this location)", metavar="DIR")
  parser.add_argument("--type", choices=("empty", "full", "simple", "all"),
                      default="full", type=str, help="what type of room to get")
  parser.add_argument("--image", choices=("albedo", "depth", "normal", "rgb_coldlight", "rgb_rawlight", "rgb_warmlight", "semantic"),
                      default="rgb_rawlight", type=str, help="what image file to get")
  parser.add_argument("--scale", default="1", type=float,
                      help = "how much to scale the images")
  parser.add_argument("--roll", default=1, type=int,
                      help = "For panoramas, translates and wraps the image \
                      e.g. 1 will give just the original image, 2 will give \
                      the original plus a 180 degree rolled version, 3 will \
                      give three evenly spaced rolled versions")
  parser.add_argument("--reflect", default=False, type=bool,
                      help="Whether to include reflected versions of each image\
                      in the final dataset")
  return parser.parse_args()

def main():

  args = parse_args()

  FULL_DATASET_DIRECTORY = args.input
  OUTPUT_DIRECTORY = args.output
  if args.type != "all":
    ROOM_TYPE = [args.type]
  else:
    ROOM_TYPE = ["empty", "full", "simple"]
  IMAGE_TO_GET = args.image
  DOWNSCALE_PERCENT = args.scale
  ROLL = args.roll
  REFLECT = args.reflect


  #add folder name to ouput directory
  OUTPUT_DIRECTORY += "Structured3D_panos_" + args.type + "_" + IMAGE_TO_GET.split(".")[0] + "_" + str(int(DOWNSCALE_PERCENT*100)) + "/"

  ground_truth = ""

  print("Copying and scaling image files...")

  i = 0
  total = len(os.listdir(FULL_DATASET_DIRECTORY))

  if not os.path.exists(OUTPUT_DIRECTORY):
    os.makedirs(OUTPUT_DIRECTORY)
    print("Created new directory", OUTPUT_DIRECTORY)
  else:
    print("Using existing directory", OUTPUT_DIRECTORY)

  for scene in os.listdir(FULL_DATASET_DIRECTORY):

    scene_id = scene[6:]

    with open(FULL_DATASET_DIRECTORY + "/" + scene + "/annotation_3d.json") as file:
      ground_truth = ground_truth + get_sqft(json.load(file), scene_id)

    for room_id in os.listdir(FULL_DATASET_DIRECTORY + "/" + scene + "/2D_rendering/"):
      for room_type in ROOM_TYPE:
        img = Image.open(FULL_DATASET_DIRECTORY + "/" + scene + "/2D_rendering/" + room_id + "/panorama/" + room_type + "/" + IMAGE_TO_GET + ".png")
        img = img.resize((int(DOWNSCALE_PERCENT * img.size[0]), int(DOWNSCALE_PERCENT * img.size[1])), PIL.Image.ANTIALIAS)
        if ROLL > 1:
          offset = img.size[0] // ROLL
          shifts = [i * offset for i in range(ROLL)]
          np_img = np.array(img)
          for shift in shifts:
            new_np_img = np.roll(np_img, shift, axis=1)
            new_img = PIL.Image.fromarray(np.uint8(new_np_img))
            new_img.save(OUTPUT_DIRECTORY + scene_id + "_" + room_id + "_" + room_type + "_" + str(shift) + ".png")
            if REFLECT:
              new_img_reflected = ImageOps.mirror(new_img)
              new_img_reflected.save(OUTPUT_DIRECTORY + scene_id + "_" + room_id + "_" + room_type + "_" + str(shift) + "_reflect.png")
        else:
          img.save(OUTPUT_DIRECTORY + scene_id + "_" + room_id + "_" + room_type + ".png")
          if REFLECT:
            img_reflected = ImageOps.mirror(img)
            img_reflected.save(OUTPUT_DIRECTORY + scene_id + "_" + room_id + "_" + room_type + "_reflect.png")

    i += 1
    print(i, "/", total, "scenes\r", end = "")

  print("Writing ground_truth.csv...")
  with open(OUTPUT_DIRECTORY + "/ground_truth.csv", "w") as ground_truth_file:
    ground_truth_file.write(ground_truth[:-1]) # [:-1] to get rid of extra newline character

  print("Sorting ground_truth.csv by ID and removing duplicates...")
  sorted_ground_truth = []

  with open(OUTPUT_DIRECTORY + "/ground_truth.csv", "r") as ground_truth_file:
    ground_truth = list(csv.reader(ground_truth_file))

    files = os.listdir(OUTPUT_DIRECTORY)
    ids_with_pics = []
    for x in files:
      if x.split('.')[1] == 'png':
        ids_with_pics.append((x.split('.')[0]))
    
    print(len(ids_with_pics))

    for line in ground_truth:
      if line[1] in ids_with_pics and not (line in sorted_ground_truth): # get rid of duplicates
        sorted_ground_truth += [line]
    sorted_ground_truth = sorted(sorted_ground_truth, key=lambda x: x[1])

  print(len(sorted_ground_truth))
  with open(OUTPUT_DIRECTORY + "/ground_truth_new.csv", "w") as file:
    writer = csv.writer(file)
    writer.writerows(sorted_ground_truth)

if __name__ == "__main__":
  main()