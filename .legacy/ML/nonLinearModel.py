import glob, os
from skimage import io, color
from skimage.transform import rescale
import torch
import torch.nn as nn
import pandas as pd
import numpy as np
import csv
from du.lib import train, get_device, center, normalize, r_squared, cross_validate, cross_validate_train, _catch_sigint

DATASET_PATH = '/Users/grantbushman/Desktop/DLFiles/Structured3D_panos_full_rgb_rawlight_25'
TRAINING_PERCENT = 0.7
NUM_IMAGES = 1000  # how many images to pull in (max 21835)
device = get_device()
print(device)

xss = torch.Tensor(NUM_IMAGES, 64, 128)

print("importing photos...")
i = 0
for file in sorted(glob.glob(DATASET_PATH + "/*.png")):
    if i < NUM_IMAGES - 1:
        img_data = color.rgb2gray(io.imread(file))
        img_data = rescale(img_data, .5, anti_aliasing=False, multichannel=False)
        xss[i] = torch.Tensor((img_data))
        i += 1
        #print(i, "/", NUM_IMAGES, "\r", end="")
    else:
        break

print(xss.size())

yss = torch.Tensor(NUM_IMAGES, 1)

with open(DATASET_PATH + "/ground_truth.csv") as csvfile:
    csvfile = list(csv.reader(csvfile))
    csvfile = sorted(csvfile, key=lambda x: x[1])
    for i in range(NUM_IMAGES):
        yss[i] = float(csvfile[i][2])

indices = torch.randperm(len(xss)).to(device)
xss = xss.index_select(0, indices)
yss = yss.index_select(0, indices)

cutoff = int(TRAINING_PERCENT * len(xss))
xss_train = xss[:cutoff]
yss_train = yss[:cutoff]
xss_test = xss[cutoff:]
yss_test = yss[cutoff:]

xss_train, xss_means = center(xss_train)
xss_train, xss_std = normalize(xss_train)

yss_train, yss_means = center(yss_train)
yss_train, yss_std = normalize(yss_train)

xss_test, _ = center(xss_test, xss_means)
xss_test, _ = normalize(xss_test, xss_std)

yss_test, _ = center(yss_test, yss_means)
yss_test, _ = normalize(yss_test, yss_std)

class NonLinearModel(nn.Module):

    def __init__(self):
        super(NonLinearModel, self).__init__()
        self.layer1 = nn.Linear(8192, 1024)
        self.layer2 = nn.Linear(1024, 1)
        # self.layer3 = nn.Linear(512, 128)
        # self.layer4 = nn.Linear(128, 32)
        # self.layer5 = nn.Linear(32, 1)

    def forward(self, xss):
        # xss = torch.unsqueeze(xss, dim=1)
        xss = self.layer1(xss.view(-1, 8192))
        xss = torch.relu(xss)
        xss = self.layer2(xss)
        # xss = self.layer3(xss)
        # xss = self.layer4(xss)
        # xss = self.layer5(xss)
        return xss


model = NonLinearModel().to(device)

criterion = nn.MSELoss()
epochs = 50
learning_rate = 0.0009
momentum = 0.99
# batchsize = len(yss_train)//4
batchsize = len(xss_train)

model = train(
    model,
    criterion,
    (xss_train, yss_train),
    test_data=(xss_test, yss_test),
    epochs=epochs,
    learn_params={"lr": learning_rate, "mo": momentum},
    graph=1
)

print("Percent correct (Training): " + str(int(r_squared(model(xss_train), yss_train) * 100)) + '%')
testcorrect = r_squared(model(xss_test), yss_test)
print("Percent Correct (Testing): " + str(int(testcorrect*100)) + '%')