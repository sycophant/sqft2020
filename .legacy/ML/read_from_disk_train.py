import glob, os
from skimage import io, color
import torch
import torch.nn as nn
import pandas as pd
import numpy as np
import csv
import random
from du.lib import train, get_device

DATASET_PATH = '/Users/grantbushman/Desktop/DLFiles/Structured3D_panos_full_rgb_rawlight_25'

device = get_device()

# create a dictionary of the csv file with key ID and value area
# this allows more time-efficient lookups
with open(DATASET_PATH + "/ground_truth.csv") as csvfile:
    csvfile = list(csv.reader(csvfile))
    area_lookup = dict()
    for row in csvfile:
        area_lookup[row[1]] = row[2]


def load_batch(dataset_path, batch_size):
    # allocate memory for batch xss and batch yss tensors
    batch_xss = torch.Tensor(batch_size, 128, 256).to(device)
    batch_yss = torch.Tensor(batch_size, 1).to(device)

    # randomly select batch_size images from the dataset path
    selected_images = random.sample(glob.glob(dataset_path + "/*.png"), batch_size)

    # load in batch xss tensor from image files
    i = 0
    for img in selected_images:
        img_data = color.rgb2gray(io.imread(img))
        batch_xss[i] = torch.Tensor((img_data[:, :]).reshape(128, 256))
        i += 1

    # load in batch yss tensor from csv file
    i = 0
    for img in selected_images:
        id = img.split("/")[-1].split(".")[0]
        batch_yss[i] = float(area_lookup[id])
        i += 1

    return batch_xss, batch_yss


class ConvolutionalModel(nn.Module):

    def __init__(self):
        super(ConvolutionalModel, self).__init__()
        self.meta_layer1 = nn.Sequential(
            nn.Conv2d(in_channels=1, out_channels=16, kernel_size=5, stride=1, padding=2),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=2, stride=2, padding=0)
        )
        self.meta_layer2 = nn.Sequential(
            nn.Conv2d(in_channels=16, out_channels=32, kernel_size=5, stride=1, padding=2),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=2, stride=2, padding=0)
        )
        self.fc_layer1 = nn.Linear(65536, 3000)
        self.fc_layer2 = nn.Linear(3000, 1)

    def forward(self, xss):
        xss = torch.unsqueeze(xss, dim=1)
        xss = self.meta_layer1(xss)
        xss = self.meta_layer2(xss)
        xss = torch.reshape(xss, (-1, 65536))
        xss = self.fc_layer1(xss)
        xss = self.fc_layer2(xss)
        return xss


model = ConvolutionalModel().to(device)

criterion = nn.MSELoss()
epochs = 20
learning_rate = 0.00001
momentum = 0.9
batchsize = 20

previous_grad = [0 for param in model.parameters()]

for epoch in range(epochs):  # train the model

    batch_xss, batch_yss = load_batch(DATASET_PATH, batchsize)

    # get the train data loss
    yss_pred = model(batch_xss)
    loss = criterion(yss_pred, batch_yss)  # compute the loss

    # print out losses
    print("epoch: {0}, train loss: {1}".format(epoch + 1, loss.item()))

    model.zero_grad()  # set the gradient to the zero vector
    loss.backward()  # compute the gradient of the loss function w/r to the weights

    # adjust the weights
    i = 0
    for param in model.parameters():
        param.data.sub_(learning_rate * (momentum * previous_grad[i] + param.grad.data))
        i += 1

    previous_grad = [param.grad.data for param in model.parameters()]
