import glob, os, csv

OUTPUT_DIRECTORY = "/home/carter/Desktop/Structured3D_panos_full_rgb_rawlight_25"

sorted_ground_truth = []
with open(OUTPUT_DIRECTORY + "/ground_truth.csv", "r") as ground_truth_file:
  ground_truth = list(csv.reader(ground_truth_file))

  files = os.listdir(OUTPUT_DIRECTORY)
  ids_with_pics = []
  for x in files:
    if x.split('.')[1] == 'png':
      ids_with_pics.append((x.split('.')[0]))
    
  print(len(ids_with_pics))

  for line in ground_truth:
    if line[1] in ids_with_pics and not (line in sorted_ground_truth): # get rid of duplicates
      sorted_ground_truth += [line]
  sorted_ground_truth = sorted(sorted_ground_truth, key=lambda x: x[1])

print(len(sorted_ground_truth))
with open(OUTPUT_DIRECTORY + "/ground_truth_new.csv", "w") as file:
  writer = csv.writer(file)
  writer.writerows(sorted_ground_truth)