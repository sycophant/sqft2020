import csv
import os

DATASET_PATH = "/home/carter/Desktop/Structured3D_panos_full_rgb_rawlight_25"

files_to_remove = []
new_ground_truth = []

with open(DATASET_PATH + "/ground_truth.csv") as csvfile:
  ground_truth = list(csv.reader(csvfile))
  for example in ground_truth:
    if float(example[2]) > 700:
      files_to_remove.append(DATASET_PATH + "/" + example[1] + ".png")
    else:
      new_ground_truth.append(example)

for img in files_to_remove:
  os.remove(img)

with open(DATASET_PATH + "/ground_truth_updated.csv", "w") as myfile:
  writer = csv.writer(myfile)
  writer.writerows(new_ground_truth)