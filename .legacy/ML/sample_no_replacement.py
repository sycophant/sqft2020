import glob
from skimage import io
import torch
import torch.nn as nn
import csv
import random
import matplotlib.pyplot as plt
from skimage.transform import rescale
from du.lib import get_device, r_squared

DATASET_PATH = '/Users/grantbushman/Desktop/DLFiles/Structured3D_panos_full_rgb_rawlight_25'
TRAINING_PERCENT = 0.9
IMG_SCALE = 0.25

img_height = int(IMG_SCALE * 128)
img_width = int(IMG_SCALE * 256)

criterion = nn.MSELoss()
epochs = 2
learning_rate = 0.0000001
momentum = 0.75
batch_size = 250

device = get_device()

# create a dictionary of the csv file with key ID and value area
# this allows more time-efficient lookups
with open(DATASET_PATH + "/ground_truth.csv") as csvfile:
    csvfile = list(csv.reader(csvfile))
    area_lookup = dict()
    for row in csvfile:
        area_lookup[row[1]] = row[2]

# get all of the images in the dataset directory
all_images = glob.glob(DATASET_PATH + "/*.png")

# the cutoff where training ends and testing begins
cutoff = int(TRAINING_PERCENT * len(all_images))

# select training and testing images
train_images = all_images[:cutoff]
test_images = all_images[cutoff:]

'''
Loads in random images from a list of image paths

images: a list of (full) image paths to load in
batch_size: how many randomly selected images to pull in
'''


def load_batch(images, batch_size=0):
    # if no parameter is given, just return the full batch
    if batch_size == 0:
        batch_size = len(images)

    # allocate memory for batch xss and batch yss tensors
    xss = torch.Tensor(batch_size, 3, img_height, img_width).to(device)
    yss = torch.Tensor(batch_size, 1).to(device)

    # select batch_size images from the given images
    selected_images = random.sample(images, batch_size)

    # load in batch xss tensor from image files
    i = 0
    for img in selected_images:
        img_data = rescale(io.imread(img), IMG_SCALE, multichannel=True)
        xss[i] = torch.Tensor((img_data[:, :, :-1]).reshape(3, img_height, img_width))
        i += 1

    # load in batch yss tensor from csv file
    i = 0
    for img in selected_images:
        img_id = img.split("/")[-1].split(".")[0]
        yss[i] = float(area_lookup[img_id])
        i += 1

    return xss, yss


class ConvolutionalModel(nn.Module):

    def __init__(self):
        super(ConvolutionalModel, self).__init__()

        self.meta_layer1 = nn.Sequential(
            nn.Conv2d(in_channels=3, out_channels=16, kernel_size=5, stride=1, padding=2),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=2, stride=2, padding=0)
        )

        self.meta_layer2 = nn.Sequential(
            nn.Conv2d(in_channels=16, out_channels=32, kernel_size=5, stride=1, padding=2),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=2, stride=2, padding=0)
        )

        self.meta_layer3 = nn.Sequential(
            nn.Conv2d(in_channels=32, out_channels=64, kernel_size=5, stride=1, padding=2),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=2, stride=2, padding=0)
        )

        self.fc_layer1 = nn.Linear(64 * int(img_height/8) * int(img_width/8), 5000)
        self.fc_layer2 = nn.Linear(5000, 100)
        self.fc_layer3 = nn.Linear(100, 1)

    def forward(self, xss):
        xss = self.meta_layer1(xss)
        xss = self.meta_layer2(xss)
        xss = self.meta_layer3(xss)
        xss = torch.reshape(xss, (-1, xss.shape[1] * xss.shape[2] * xss.shape[3]))
        xss = self.fc_layer1(xss)
        xss = self.fc_layer2(xss)
        return self.fc_layer3(xss)


model = ConvolutionalModel().to(device)

previous_grad = [0 for param in model.parameters()]

past_losses = []
avg_past_losses = []

for epoch in range(epochs):  # train the model

    for i in range(len(train_images) // batch_size):

        # chunk through train_images
        start_index = i * batch_size
        end_index = (i + 1) * batch_size
        batch_train_images = train_images[start_index:end_index]

        # get chunked out images
        batch_xss, batch_yss = load_batch(batch_train_images)

        # get the train data loss
        yss_pred = model(batch_xss)
        loss = criterion(yss_pred, batch_yss)  # compute the loss

        # calculate average of last 50 losses
        past_losses.append(loss.item())
        avg_loss = sum(past_losses[-50:]) / len(past_losses[-50:])
        avg_past_losses.append(avg_loss)

        # print info
        print("epoch: {} ({} / {}) loss: {:<20} avg of last 50: {}"
            .format(
            epoch + 1,
            i,
            len(train_images) // batch_size,
            loss.item(),
            avg_loss
        )
        )

        model.zero_grad()  # set the gradient to the zero vector
        loss.backward()  # compute the gradient of the loss function w/r to the weights

        # adjust the weights
        i = 0
        for param in model.parameters():
            param.data.sub_(learning_rate * (momentum * previous_grad[i] + param.grad.data))
            i += 1

        previous_grad = [param.grad.data for param in model.parameters()]

    random.shuffle(train_images)

# save the model
torch.save(model.state_dict(), "/Users/grantbushman/Desktop/DLFiles/model.pyt")

# load the model in again on the CPU to run the testing data
device = 'cpu'
model = ConvolutionalModel().to(device)
model.load_state_dict(torch.load("/Users/grantbushman/Desktop/DLFiles/model.pyt"))
model.eval()

# calculate the r squard of the testing data
print("Calculating testing r squared")
test_xss, test_yss = load_batch(test_images)
print("Testing r squared:", r_squared(model(test_xss), test_yss))

# plot average of past 50 losses over time
plt.plot(range(len(avg_past_losses)), avg_past_losses)
plt.show()