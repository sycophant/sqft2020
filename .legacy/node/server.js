//Import the necessary libraries/declare the necessary objects
var express = require("express");
var myParser = require("body-parser");
var fs = require('fs');
var multer = require('multer');
var fs = require('fs');

var pwd = "/home/carter/SyntaxErrors-2019/node/"

var app = express();

app.use(myParser.json({extended : true}));

/**
 * Prints out the given argument to console.log along with the datetime
 */
function writeOut(myString){
    console.log(new Date().toLocaleString() + "  " + myString);
}

/**
 * Storage to be used by multer to save the file
 */
var Storage = multer.diskStorage({
    destination: function(request, file, callback) {
        callback(null, pwd + "images/");
    },
    filename: function(request, file, callback) {
        callback(null, request.body.name + "_" + file.originalname);
    }
});

/**
 * The multer object which will be saving the file
 * .single('file') indicates that we are only accepting a single file at a time
 * not an array of files
 */
var upload = multer({ storage: Storage }).single('file'); //Field name 'file'

/**
 * ******************************* API ENDPOINTS *******************************
 */

/**
 * POST endpoint for sending estimates json to the server
 * Writes json it receives to estimates.json file
 */
app.post("/estimates", function(request, response) {
    writeOut("Recieved POST for estimates"); 
    response.send("folders.json received");
    let data = JSON.stringify(request.body);
    fs.writeFileSync(pwd + 'estimates.json', data);
    writeOut("Wrote estimates json");
});

/**
 * POST endpoint for sending folders json to the server
 * Writes json it receives to folders.json file
 */
app.post("/folders", function(request, response) {
    writeOut("Recieved POST for folders"); 
    response.send("folders.json received"); 
    let data = JSON.stringify(request.body);
    fs.writeFileSync(pwd + 'folders.json', data);
    writeOut("Wrote folders json");
});

/**
 * GET endpoint for getting estimates json from the server
 * Responds with the estimates json
 */
app.get("/estimates", function(request, response) {
    writeOut("Recieved GET for estimates");
    let rawdata = fs.readFileSync(pwd + 'estimates.json');
    response.send(rawdata);
});

/**
 * GET endpoint for getting folders json from the server
 * Responds with the folders json
 */
app.get("/folders", function(request, response) {
    writeOut("Recieved GET for folders");
    let rawdata = fs.readFileSync(pwd + 'folders.json');
    response.send(JSON.parse(rawdata));
})

/**
 * GET endpoint for getting a list of all of the images from the server
 * Responds with a json file containing the name of every image
 * The images can then be retrieved by calling the /images/:file resource below
 */
app.get("/listimages", function(request, response) {
    writeOut("Recieved GET for listimages");
    response.send(fs.readdirSync(pwd + "images/"));
});

/**
 * GET endpoint for getting a single image from the server
 * Responds with a single image - as an actual .png file
 */
app.get("/images/:file", function(request, response) {
    writeOut("Recieved GET for image" + request.params.file);
    response.sendFile(pwd + "images/" + request.params.file);
});

/**
 * POST endpoint for sending an image to the server
 * Saves the image in the images folder
 * 
 * It is *VERY IMPORTANT* that in the POST request form-data, the name parameter
 * comes BEFORE the file parameter (which contains the actual image file)
 * Because those parameters are processed in order, this insures that the name
 * will be available when we go to save the file
 */
app.post("/upload", function(request, response) {
    writeOut("Recieved POST for single upload");
    upload(request, response, function(err) {
        if (err) {
            return response.send("ERROR: Upload failed");
        }
        return response.send("File uploaded");
    });
});

//Start the server and make it listen for connections on port 3000
app.listen(3000);

//Log to console that server has started
writeOut("POST server running on port 3000")
