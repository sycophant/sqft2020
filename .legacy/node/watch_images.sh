# monitors images folder for new files, calls python script when one shows up
# https://unix.stackexchange.com/questions/24952/script-to-monitor-folder-for-new-files

inotifywait -m /home/carter/SyntaxErrors-2019/node/images -e create -e move |
    while read path action file; do
        printf date + "running command python3 /home/carter/git_repositories/SyntaxErrors-2019/node/run_model.py --input " $file
        python3 /home/carter/SyntaxErrors-2019/node/run_model.py --input $file
    done
