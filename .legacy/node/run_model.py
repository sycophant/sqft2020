# Copyright (c) 2020 Carter Sifferman and Grant Bushman

# Runs the estimate model on a single image
# Result is written to estimates.json or folders.json
# Uses the name of the input file (given with --input argument) to search for
# matching object in estimates.json and, if not found there, folders.json
# automatically called by "watch_images.sh" script when a new file shows up
# in images folder

import torch
import torch.nn as nn
import os
from skimage.transform import rescale
from skimage import io
import argparse
import json

def parse_args():
  parser = argparse.ArgumentParser(description="Runs floor space estimate")
  parser.add_argument("--input", required=True,
                      help="single input image", metavar="DIR")
  return parser.parse_args()

class ConvolutionalModel(nn.Module):

  def __init__(self):

    super(ConvolutionalModel, self).__init__()

    self.meta_layer1 = nn.Sequential(
      nn.Conv2d(in_channels=3, out_channels=16, kernel_size=5, stride=1, padding = 2),
      nn.ReLU(),
      nn.MaxPool2d(kernel_size = 2, stride = 2, padding = 0)
    )

    self.meta_layer2 = nn.Sequential(
      nn.Conv2d(in_channels=16, out_channels=32, kernel_size=5, stride=1, padding=2),
      nn.ReLU(),
      nn.MaxPool2d(kernel_size = 2, stride = 2, padding = 0)
    )

    self.meta_layer3 = nn.Sequential(
      nn.Conv2d(in_channels=32, out_channels=64, kernel_size=5, stride=1, padding=2),
      nn.ReLU(),
      nn.MaxPool2d(kernel_size = 2, stride = 2, padding = 0)
    )

    self.fc_layer1 = nn.Linear(int(32768 * 1 / 8 * 1 / 8 * 4), 5000)
    self.fc_layer2 = nn.Linear(5000, 100)
    self.fc_layer3 = nn.Linear(100, 1)

  def forward(self, xss):
    xss = self.meta_layer1(xss)
    xss = self.meta_layer2(xss)
    xss = self.meta_layer3(xss)
    xss = torch.reshape(xss, (-1, xss.shape[1]*xss.shape[2]*xss.shape[3]))
    xss = self.fc_layer1(xss)
    xss = self.fc_layer2(xss)
    return self.fc_layer3(xss)

def update_estimate(name, size):
  # keep track of if we have found the matching est. yet
  found = False

  # read in estimates json to search through for an estimate of same name
  with open('estimates.json') as estimates_file:
    estimates = json.load(estimates_file)
    for est in estimates:
      if est['name'] == name:
        est['size'] = size
        found = True
        break
  # open again in write mode to re-write
  if found:
    with open('estimates.json', 'w') as estimates_file:
      json.dump(estimates, estimates_file)

  else:
    # do the same with folders.json
    with open('folders.json') as folders_file:
      folders = json.load(folders_file)
      for fold in folders:
        for est in fold['estimates']:
          if est['name'] == name:
            est['size'] = size
            found = True
            break
    # open again in write mode to re-write
    if found:
      with open('folders.json', 'w') as folders_file:
        json.dump(folders, folders_file)
    else:
        print("estimate " + name + " not found in json")

def main():

  # change directory to directory of current file
  os.chdir(os.path.dirname(os.path.abspath(__file__)))

  args = parse_args()

  INPUT = "images/" + args.input
  MODEL = "serialized_model.pyt"
  yss_mean = 141.94385429381174
  yss_stdev = 136.1439866831544
  IMG_SCALE = 1 / 8
  img_height = int(IMG_SCALE * 256)
  img_width = int(IMG_SCALE * 512)

  # load in and run the model
  device = torch.device('cpu')
  model = ConvolutionalModel()
  model.load_state_dict(torch.load(MODEL, map_location=device))
  
  img_data = rescale(io.imread(INPUT), IMG_SCALE, multichannel=True)
  model_input = torch.Tensor((img_data[:,:,:-1]).reshape(1,3,img_height,img_width))
  
  model_output = model.forward(model_input)*yss_stdev+yss_mean

  print(INPUT.split("/")[1].split(".")[0])
  print(model_output.item())
  update_estimate(INPUT.split("/")[1].split(".")[0], model_output.item())

if __name__ == "__main__":
  main()
