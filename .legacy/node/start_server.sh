#!/bin/bash
 
log=server_log.txt
 
# create log file or overrite if already present
printf "" >> $log
printf "Server started - " >> $log
 
# append date to log file
date >> $log

# run node server and write to log
node server.js & >> $log
bash ./watch_images.sh & >> $log
