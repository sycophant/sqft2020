# SQFT2020 Project
The 2020-21 continuation of DUCS's Square Footage Estimation senior RnD project.

## Preliminaries
Our application test requires both _nodejs_ and _openjdk_ to run. Make sure to install both beforehand.

**For Linux users**, you can get _node_ and _openjdk_ in your respective package managers.

**For Windows users**, the fastest solution is to install _chocolatey_ first. Do this in the command line:
```
Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
```
Then run:
```
choco install openjdk nodejs
```
**For Mac users**, visit https://jdk.java.net/14/ and download the JDK tar.gz file

To begin, locate the download. Likely in the Downloads folder:
```
cd ~/Downloads
```

_*Note: Newer Macs using Safari will decompress tar.gz on the fly. Adjust this next command depending on the file downloaded_

Next, untar the the file by issuing:
```
tar xf openjdk-14.0.2_osx-x64_bin.tar
```
To finish installing, move the untarred file to a system-wide Java directory by running:
```
sudo mv jdk-14.jdk /Library/Java/JavaVirtualMachines/
```
Check to see if things worked out with the command:
```
Java --version
```

To install _nodejs_ visit their official website and download the installer pkg for Mac at https://nodejs.org/en/download/


## Live Testing our Application

Once set with the dependencies, run the following (UNIX users must prefix sudo):

```
npm install -g expo-cli
```

Navigate to project folder and run:

```
npm install
expo start
```

Get the expo app on the store to do live testing on your device (available for both android and iOS).

On your device, you can use the QR code scanner to get hooked into a live instance of your React Native environment. QR code is prominently displayed in bottom left corner of web server's startup page.

__WARNING:__ Do not re-initialize the repo as shown in the reactnative documentation
