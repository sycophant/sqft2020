# classifier.py		    Author(s): B. Bena, R. McKinney, S. Simmons
# Pulled largely from Simmons' DataLoader tutorial
#	on the DL@DU Page. Code is adapted to fit the
#	2020 SQFT AI Team's first task of room classification


# Imports for Dataloading and Model Training
from skimage import io
import pandas as pd
import pathlib, torch
from torch.utils.data import Dataset, DataLoader
import torchvision.transforms
import torch.nn as nn
import du.lib as dulib
import du.utils, du.conv.models
import csv
import random

# set up command-line switches
parser = du.utils.standard_args('Convolutional model.', epochs=10, lr=0.001,
	mo=0.975, bs=20, prop=0.8, channels=(1,24,32,64,128), widths = (448,224,112,56,), gpu=(1,),
	graph=0, verb=2, cm=True)
p = parser.add_argument
p("-ser","--serialize",help="save the trained model",action="store_true", default = True)
p("-load","--load",help="load a trained model",action="store_true", default = False)
args = parser.parse_args()

# Where the Files are stored
imagedir = '/home/sharedData/Shanghai/Structured3D_panos_full_rgb_rawlight_25/'
csv_file = 'ground_truth.csv'

print()
print("Opening CSV...")

# Grab labels from csv
#        Contains a loop that will create a label:int dictionary
#           that allows us to work around our labels being strings
#           (Tensors can not be strings)
with open(imagedir + csv_file) as csvfile:
	csvfile = list(csv.reader(csvfile))
	labels = set()
	for i in range(len(csvfile)):
		labels.add(csvfile[i][0])
	labels = list(labels)
	label2int = {}
	for i, label in enumerate(labels):
		label2int[label] = i


# Invert label2int for later us in tensor conversion
int2label = {v: k for k, v in label2int.items()}

def tensorCat(tensor):
	rotations = [0,.25,.50,.75]
	rot_factor = random.choice(rotations)
	index = int(len(tensor) * rot_factor)
	tensor_front = tensor[:,:index]
	tensor_back = tensor[:,index:]
	new_tensor = torch.cat([tensor_back,tensor_front], dim=1)
	return new_tensor

# subclass Dataset appropriately for the training data
class ImageData(Dataset):
    # Attributes
	def __init__(self, imagedir, dataframe, *transforms):
		self.frame = dataframe
		self.dir = imagedir
		self.transforms = transforms
    # Just the length
	def __len__(self):
		return len(self.frame)

    # Pairs the file name with each of the labels to create tuples for use
	def __getitem__(self, idx):
		imagefile = self.dir + self.frame.iloc[idx, 1] + '.png'
		imagelabel = label2int[self.frame.iloc[idx, 0]]
		return tuple(tform(xs) for tform, xs in zip(
			self.transforms,
			(io.imread(imagefile), imagelabel)
			) if tform is not None)

# Our images are rather large and four dimensional.
#       So, we first convert them to a PIL tensor. Then we resize.
#       Then we go grayscale to lop off some unneeded dimensions
#       Finally, we convert them to a tensor and squeeze them to [32,64] tensors
image_transform = torchvision.transforms.Compose([
	torchvision.transforms.ToPILImage(),
	torchvision.transforms.Resize((64,128)),
	torchvision.transforms.Grayscale(),
	torchvision.transforms.ToTensor(),
	torchvision.transforms.Lambda(lambda xs: tensorCat(xs)),
	#torchvision.transforms.RandomHorizontalFlip(p=0.75),
	torchvision.transforms.Lambda(lambda xs: xs.squeeze(0))])

# get the all of the image data in the form of a pandas dataframe
df = pd.read_csv(imagedir+csv_file)

# Dropping some unneccesary data
#
#	TO-DO: Fix the CSV so that 'dining room' is not the header
#
df.drop(df.loc[df['dining room']=='undefined'].index, inplace=True)
df.drop(df.loc[df['dining room']=='study'].index, inplace=True)
df.drop(df.loc[df['dining room']=='garden'].index, inplace=True)
df.drop(df.loc[df['dining room']=='store room'].index, inplace=True)
df.drop(df.loc[df['dining room']=='corridor'].index, inplace=True)
df.drop(df.loc[df['dining room']=='study'].index, inplace=True)
df.drop(df.loc[df['dining room']=='dining room'].index, inplace=True)

# split out training and testing dataframes     (Default: 80/20 - Testing/Training Split)
train_df = df.sample(frac = args.prop)
test_df = df.drop(train_df.index)

# Leveraging PyTorch's data loader class to keep from borking our machine
train_data = ImageData(imagedir, train_df, image_transform)
train_loader = DataLoader(dataset=train_data, batch_size=args.bs)

print()
print("Computing Means & St. Devs...")

# DUlib function for (online) computation of the means and stdevs
#       This grabs batches from the CPU and tosses them to the GPU for training.
feats_means, feats_stdevs = dulib.online_means_stdevs(train_loader)

# add standardization to features transform and create a transform for targets
feat_transform = torchvision.transforms.Compose([
	    image_transform,
	    torchvision.transforms.Lambda(
        lambda xs: dulib.standardize(xs,means=feats_means,stdevs=feats_stdevs))])

targ_transform = torchvision.transforms.Lambda(lambda xs: torch.tensor(xs))

# re-instance TrainData using targs_transform so we output (feature, target) pairs
train_data = ImageData(imagedir, train_df, feat_transform, targ_transform)
train_loader = DataLoader(dataset=train_data, batch_size=args.bs, num_workers=4, shuffle=True)

print()
print("Extracting test features and targets...")

# Extract the test data (which is typically of manageable size) as tensors
#      This will take our tensors of each test image and stack them into a tensor for the model.
#       It also grabs the int value of each label and converts them to tensors - again, stacked for the model
test_feats = torch.stack([feat_transform(io.imread(imagedir+filename+'.png'))\
	for filename in test_df.iloc[:,1]])
test_targs = torch.stack([targ_transform(label2int[label]) for label in test_df.iloc[:, 0].values])

print()
print("Creating model...")

# create a convolutional model
conv_model =  du.conv.models.ConvFFNet(
	in_size = (64, 128),
    n_out = len(labels),
	channels = args.channels,
	widths = args.widths,
    means = feats_means,
    stdevs = feats_stdevs)

# Load model if necessary
if args.load:
    conv_model.load_state_dict(torch.load("classifier.pyt"))

# train the model
model = dulib.train(
	model = conv_model,
	crit = nn.NLLLoss(),
	train_data = train_loader,
	test_data = (test_feats, test_targs) if args.prop < 1 else None,
	args = args)

# evaluate on test data
#       This uses the int2label dict for labelling the confusion matrix properly
if args.prop < 1:
	print('On test data: {:.2f}% correct.'.format(100*dulib.confusion_matrix(
	(model, test_feats), test_targs, torch.arange(len(labels)), class2name = int2label, show = args.cm, gpu = -2)))


# Think about switching the GPU argument above to use the CPU to toss batches into the GPU of the test data
# 	once a dataloader class has been implemented for the test data
# Think about changing the convulational net's pooling and windows sizes for testing

# Adding serialization functionality
if args.serialize:
  print('saving model ... ', end='')
  torch.save(conv_model.state_dict(), "classifier.pyt")
  print('done.')
